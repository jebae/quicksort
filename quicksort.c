#include <stdio.h>

void quickSort(int arr[], int left, int right)
{
    int i=left, j=right;
    int pivot = arr[(left + right)/2];
    int temp;

    while (i <= j)
    {
        while (arr[i] < pivot)
            i++;
        while (arr[j] > pivot)
            j--;

        if (i <= j)
        {
            temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
            i++;
            j--;
        }
    }
    if (i < right)
        quickSort(arr, i, right);
    if (j > left)
        quickSort(arr, left, j);
}

int main()
{
    int arr[10] = { 5,7,3,1,9,2,6,4,10,8 };
    int size = sizeof(arr)/sizeof(int);
    quickSort(arr, 0, size-1);

    for (int i=0; i < size; i++)
        printf ("%d ", arr[i]);
    return 0;
}
